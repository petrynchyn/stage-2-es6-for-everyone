import { showModal } from "./modal"
import { createFighterImage } from "../fighterPreview"
import { renderArena } from '../arena';

export function showWinnerModal(winner, selectedFighters) {
  // call showModal function 

  const imgElement = createFighterImage(winner)
  imgElement.style.width = "fit-content"
  imgElement.style.alignSelf = "center"

  showModal({title: `${winner.name.toUpperCase()} is WINNER!`, bodyElement: imgElement, onClose: () => renderArena(selectedFighters)})
}
